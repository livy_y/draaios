# Draai OS: Embedded Phone Project
DraaiOS is an embedded systems project focused on creating a fully functional phone. The project is built using ESP-IDF, Espressif's official IoT development framework. It incorporates modular components to handle various hardware and software functionalities.

---

## Getting started

You can try this project your self by following [these instructions](documentation/Trying-the-project.md).

---

## Features

- **Display Control**: Manages the phone's screen, including rendering graphics and text using an ILI9341 display driver.
- **GSM Interfacing [planned]**: Use your sim-card to call, browse (the very modern) 3G network and send messages.
- **Expandable storage [planned]**: Add an micro-SD-card slot so you can add as much storage as you want.

---

## Project Structure
Here's an overview of the project structure:

```plaintext

├── components/              # Custom project components
│   ├── display/             # Component to manage display
│   └── (Other components)
├── main/                    # Application code
│   ├── main.c               # Entry point of the application
│   ├── idf_component.yml    # Main component's dependencies
│   └── CMakeLists.txt       # Build configuration for main
├── CMakeLists.txt           # Top-level build configuration
└── README.md                # Project documentation (this file)
```

---

## Components

### 1. Display Component
- **Description**: Manages display functionalities and provides APIs for rendering graphics on an ILI9341 screen.

### 2. Main Application
- **Description**: The core logic that integrates all components to deliver the functionality of the phone.

---

## Contributing
Contributions are welcome! Please follow these steps:
1. Fork the repository.
2. Create a new feature branch:
   ```bash
   git checkout -b feature-name
   ```
3. Commit your changes:
   ```bash
   git commit -m "Add feature-name"
   ```
4. Push the branch:
   ```bash
   git push origin feature-name
   ```
5. Submit a pull request.

---

## License
This project is licensed under the [GPL-3 license](LICENSE).

---

## Contact
For questions or feedback, please reach out to **[liv.cool@ik.me](mailto:liv.cool@ik.me)**.
# Trying the software on your setup

Follow these steps to download the pre-built firmware binaries and flash them to your ESP32 device:

---

## Step 1: Download the Firmware
1. Go to the **CI/CD > Pipelines** section of this repository.
2. Locate the latest successful pipeline and click on it.
3. In the **Jobs** tab, select the `build` job.
4. Download the artifact archive (a `.zip` file) from the **Job Artifacts** section.
5. Extract the downloaded `.zip` file to access the firmware binaries:
   - `bootloader.bin`
   - `partition-table.bin`
   - `firmware.bin`

---

## Step 2: Install ESPTool
If you haven't already installed `esptool.py`, install it using pip:

```bash
pip install esptool
```

---

## Step 3: Flash the Firmware to Your ESP32
Connect your ESP32 to your computer via USB and find the serial port it is connected to (e.g., `/dev/ttyUSB0` on Linux, `COM3` on Windows). Then, run the following command to flash the firmware:

```bash
esptool.py --chip esp32 --port <YOUR_SERIAL_PORT> --baud 460800 write_flash \
  0x1000 bootloader.bin \
  0x8000 partition-table.bin \
  0x10000 firmware.bin
```

- Replace `<YOUR_SERIAL_PORT>` with the appropriate serial port for your device.

---

## Step 4: Verify the Flashing Process
After flashing, the ESP32 should automatically reboot and run the new firmware. You can use a serial monitor (e.g., `screen` or `minicom`) to view the output:

```bash
screen <YOUR_SERIAL_PORT> 115200
```

---

## Troubleshooting
- Ensure the correct serial port is specified.
- If the ESP32 doesn't boot, press the reset button or try erasing the flash memory before flashing:
  ```bash
  esptool.py --chip esp32 --port <YOUR_SERIAL_PORT> erase_flash
  ```
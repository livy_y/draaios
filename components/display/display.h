#ifndef DISPLAY_DRIVER_H
#define DISPLAY_DRIVER_H

#include <esp_log.h>
#include <esp_lcd_panel_io.h>
#include <esp_lcd_panel_vendor.h>
#include <esp_lcd_panel_ops.h>
#include <ili9341_driver.h>
#include "sdkconfig.h"
#include <lvgl.h>
#include <driver/gpio.h>
#include <driver/ledc.h>
#include <driver/spi_master.h>
#include <esp_timer.h>

// Uncomment the following line to enable using double buffering of LVGL color
// data.
// #define USE_DOUBLE_BUFFERING 1

// Display configuration constants
#define DISPLAY_HORIZONTAL_PIXELS    240
#define DISPLAY_VERTICAL_PIXELS      320
#define FLIPPED_Y_AXIS_DISPLAY       true
#define DISPLAY_COMMAND_BITS         8
#define DISPLAY_PARAMETER_BITS       8
#define DISPLAY_REFRESH_HZ           40000000
#define DISPLAY_SPI_QUEUE_LEN        10
#define SPI_MAX_TRANSFER_SIZE        32768

// LVGL buffer size and update period
#define LV_BUFFER_SIZE               (DISPLAY_HORIZONTAL_PIXELS * 25)
#define LVGL_UPDATE_PERIOD_MS        5

// Backlight control constants
#define BACKLIGHT_LEDC_MODE           LEDC_LOW_SPEED_MODE
#define BACKLIGHT_LEDC_CHANNEL        LEDC_CHANNEL_0
#define BACKLIGHT_LEDC_TIMER          LEDC_TIMER_1
#define BACKLIGHT_LEDC_TIMER_RESOLUTION LEDC_TIMER_10_BIT
#define BACKLIGHT_LEDC_FREQUENCY     5000

// Function declarations
void initialize_spi(void);
void initialize_display(void);
void initialize_lvgl(void);
void display_brightness_set(int brightness_percentage);
void display_brightness_init(void);

#endif // DISPLAY_DRIVER_H
/*
 * SPDX-FileCopyrightText: 2022 atanisoft (github.com/atanisoft)
 *
 * SPDX-License-Identifier: MIT
 */

#include <driver/gpio.h>
#include <driver/ledc.h>
#include <driver/spi_master.h>
#include <esp_err.h>
#include <esp_freertos_hooks.h>
#include <display.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <lvgl.h>
#include <stdio.h>


static lv_style_t style_screeneen;

void create_demo_ui()
{
    lv_obj_t *screen = lv_display_get_screen_active(NULL);

    // Set the background color of the display to black.
    lv_style_init(&style_screeneen);
    lv_style_set_bg_color(&style_screeneen, lv_color_black());
    lv_obj_add_style(lv_screen_active(), &style_screeneen, LV_STATE_DEFAULT);

}

void app_main()
{
    display_brightness_init();
    display_brightness_set(0);
    initialize_spi();
    initialize_display();
    initialize_lvgl();
    create_demo_ui();
    display_brightness_set(75);

    while (1)
    {
        vTaskDelay(pdMS_TO_TICKS(10));
        lv_timer_handler();
    }
}